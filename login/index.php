<?php
    if(isset($_GET["submit"]) && $_GET["submit"] == 'true') {
        if (isset($_POST["username"]) && isset($_POST["password"])) {
            require_once("../classes/user.php");
            $user = new User();
            $login = $user->checkLogin($_POST["username"], $_POST["password"]);
            if ($login["success"]) {
                if (isset($login["user"]["email"]))
                    $email = $login["user"]["email"];
                else
                    $email = null;
                require_once("../classes/gravatar.php");
                $gravatarHandler = new Gravatar($email);
                require_once('../classes/session.php');
                $sessionHandler = new Session();
                $sessionArgs = Array(
                    Array(
                            "key"=>"username",
                            "value"=>$login["user"]["username"]
                    ),
                    Array(
                            "key"=>"userId",
                            "value"=>$login["user"]["userId"]
                    ),
                    Array(
                            "key"=>"pfp",
                            "value"=>$gravatarHandler->getGravatar()
                    ),
                    Array(
                            "key"=>"loggedIn",
                            "value"=>true
                    ),
                    Array(
                            "key"=>"displayName",
                            "value"=>"username"
                    )
                );
                $sessionHandler->createSession($sessionArgs);
                $user->updateDisplayName($login["user"]);
                $redirect = "../home";
                if (isset($_COOKIE["redirect"])) {
                    $redirect = $_COOKIE["redirect"];
                    setcookie("redirect", null, 1);
                }
                Header("Location: " . $redirect . "?toast=".$login["success"]."&message=".$login["message"]);
                exit();
            } else {
                session_destroy();
                Header("Location: ./?success=false&message=" . $login["message"]);
                exit();
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Title -->
    <title>Login - Blog</title>

    <!-- Icon -->
    <link rel="icon" type="img/png" href="../assets/logo.png">

    <!-- Meta -->
    <meta name="author" content="sgart">
    <meta name="description" content="Blog">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="../css/index.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- JS to check data -->
    <script src="../scripts/validation.js" type="text/javascript"></script>

    <!-- JS for theming -->
    <script src="../scripts/utils.js" type="text/javascript"></script>

    <!-- JS for toasts -->
    <script src="../scripts/materialize.min.js" type="text/javascript"></script>
</head>
<body class="color--primary">
    <main>
        <div class="row">
            <div class="col s0 m3 hide-on-med-and-down"></div>
            <div class="col s12 m12 l6">
                <div class="row hide-on-small-and-down"></div>
                <div class="card color--secondary">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12" method="post" onsubmit="return login(this)" action="../login/?submit=true">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="username" name="username" type="text" class="color--primary__text">
                                        <label for="username">Username</label>
                                        <span class="helper-text color--secondary__text" data-error="Username cannot be empty!"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="password" name="password" type="password" class="color--primary__text">
                                        <label for="password">Password</label>
                                        <span class="helper-text color--secondary__text" data-error="Password cannot be empty!"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <button class="waves-effect waves-light btn color__button right" type="submit"><i class="material-icons left">person</i>Log in</button>
                                    <a class="btn waves-effect waves-light btn color__button" href="<?= $_SERVER['HTTP_REFERER'] ?>"><i class="material-icons left">arrow_back</i>Back</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php
    if (isset($_GET["success"], $_GET["message"]) && $_GET["success"] == 'false'):
        ?>
        <script>
            M.toast({html: '<?= $_GET["message"] ?>', classes: 'aurora-red'})
        </script>
    <?php
    endif;
    ?>
</body>
</html>
