FROM php:8.2.2RC1-fpm-bullseye
COPY . /var/www/html/
RUN echo "output_buffering = 1" >> /usr/local/etc/php/conf.d/php.ini
RUN docker-php-ext-install pdo && docker-php-ext-enable pdo
