<?php
    function isToday($date, $datenow){
        return date_format($date ,"d.m.Y") == date_format($datenow ,"d.m.Y");
    }

    function wasYesterday($date, $datenow){
        if (date_format($date ,"m.Y") == date_format($datenow ,"m.Y") && date_format($date ,"d") == date_format($datenow ,"d")-1){
            return true;
        }
        else if (date_format($date ,"m") == "12" && date_format($datenow ,"m") == "01" && date_format($date ,"d") == "31" && date_format($datenow ,"d") == "01"){
            return true;
        }
        return false;
    }

    function isTomorrow($date, $datenow){
        if (date_format($date ,"m.Y") == date_format($datenow ,"m.Y") && date_format($date ,"d") == date_format($datenow ,"d")+1){
            return true;
        }
        else if (date_format($date ,"m") == "01" && date_format($datenow ,"m") == "12" && date_format($date ,"d") == "01" && date_format($datenow ,"d") == "31"){
            return true;
        }
        return false;
    }

    function getDynamicTimestamp(DateTime $date){
        $datenow = date_create();
        if (isToday($date, $datenow)){
            $timestamp = "Today at ".date_format($date ,"H:i");
        }
        elseif (wasYesterday($date, $datenow)){
            $timestamp = "Yesterday at ".date_format($date ,"H:i");
        }
        elseif (isTomorrow($date, $datenow)){
            $timestamp = "Tomorrow at ".date_format($date ,"H:i");
        }
        else{
            $timestamp = date_format($date ,"d. M Y");
        }
        return $timestamp;
    }
