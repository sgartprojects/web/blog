function search() {
    // Declare variables
    var input, filter, list, items, value, i, txtValue;
    input = document.getElementById('searchTerm');
    filter = input.value.toUpperCase();
    list = document.getElementById("search-list");
    items = list.getElementsByClassName('search-item');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < items.length; i++) {
        value = items[i].getElementsByTagName("span")[0];
        txtValue = value.textContent || value.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            items[i].style.display = "";
        } else {
            items[i].style.display = "none";
        }
    }
}