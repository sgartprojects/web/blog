<?php
session_start();
if (!isset($_GET["id"])) {
    header("Location: ../home");
    exit();
}

if (!isset($_GET["loc"])){
    $loc = "home";
}
else {
    $loc = $_GET["loc"];
}

include("../classes/vote.php");
include("../classes/user.php");
if (isset($_SESSION["userId"])){
    $user = new User();
    $vote = new Vote($_GET["id"], $_SESSION["userId"]);
    $vote->downvote();
    header("Location: ../".$loc."/#".$_GET["id"]);
}
else
    header("Location: ../".$loc."/?success=false&message=You need to be logged in to downvote posts!");
