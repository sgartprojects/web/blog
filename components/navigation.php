<?php
if (!isset($user)) {
    include("../classes/user.php");
    $user = new User();
}
?>
<header>
    <nav class="color--secondary hide-on-large-only">
        <a href="" data-target="slide-out" class="sidenav-trigger white-text"><i class="material-icons">menu</i></a>
    </nav>
</header>
<ul id="slide-out" class="sidenav sidenav-fixed color--secondary">
    <li>
        <div class="user-view">
            <div class="background">
                <img src="../assets/user_background.jpg" alt="Profile Background">
            </div>
            <img class="circle center-block" alt="Profile Picture" src="<?php
            if (isset($_SESSION['pfp']) && $_SESSION['pfp'] != null){
                echo $_SESSION['pfp'];
            }
            else{
                echo 'https://gravatar.com/avatar/?d=identicon';
            }
            ?>">
            <span class="white-text name center-align">
                        <?php
                        if (isset($_SESSION["displayName"])){
                            echo $_SESSION["displayName"];
                        }
                        else {
                            echo "Anonymous";
                        }
                        ?>
                    </span>
            <span class="email"></span>
        </div>
    </li>
    <li><a href="../home" class="color--primary__text"><i class="material-icons color--primary__text">home</i>Home</a></li>
    <li><div class="divider"></div></li>
    <?php
    if (isset($_SESSION["loggedIn"])):
    ?>
    <li><a href="../user/<?= $_SESSION["username"] ?>" class="color--primary__text"><i class="material-icons color--primary__text">chat_bubble</i>My posts</a></li>
    <li><a href="../settings" class="color--primary__text"><i class="material-icons color--primary__text">settings</i>Settings</a></li>
    <li><div class="divider"></div></li>
    <?php
    if ($user->isAdmin($_SESSION["username"])):
    ?>
    <li><a href="../users" class="color--primary__text"><i class="material-icons color--primary__text">group</i>Users</a></li>
    <?php
    endif;
    ?>
    <li><div class="divider"></div></li>
    <li><a href="../faq" class="color--primary__text"><i class="material-icons color--primary__text">question_answer</i>FaQ</a></li>
    <li><div class="divider"></div></li>
    <li><a href="../logout" class="color--primary__text"><i class="material-icons color--primary__text">person</i>Log out</a></li>
    <?php
    else:
    ?>
    <li><a href="../faq" class="color--primary__text"><i class="material-icons color--primary__text">question_answer</i>FaQ</a></li>
    <li><div class="divider"></div></li>
    <li><a href="../login" class="color--primary__text"><i class="material-icons color--primary__text">person</i>Log in</a></li>
    <?php
    endif;
    ?>
</ul>