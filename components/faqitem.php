<?php
    if (!isset($item)){
        header("Location: ../home");
    }
    $item = $item->getData();
    $icon = $item["icon"];
    if ($icon == ""){
        $icon = "question_answer";
    }
?>

<li id="<?= $item["name"] ?>" class="search-item">
    <div class="collapsible-header color--secondary color--primary__text"><i class="material-icons"><?= $icon ?></i><span class="search-value"><?= $item["title"] ?></span></div>
    <div class="collapsible-body color--secondary"><span class="color--primary__text search-value"><?= $item["description"] ?></span></div>
    <span class="hide search-value"><?= $item["searchTags"] ?></span>
</li>