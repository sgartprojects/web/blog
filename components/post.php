<?php
    if (!isset($data)){
        header("Location: ../home");
        exit();
    }
    $timestamp = getDynamicTimestamp($data["createdAt"]);
?>

<div class="col s12 m6 offset-m3">
    <a href="../post/?id=<?= $data["postId"] ?> ">
        <div class="card big color--secondary hoverable" id="<?= $data["postId"] ?>">
            <div class="card-content color--primary__text">
                <a href="../user/<?= $data["user"]["username"] ?>" class="color--secondary__text"><?= $data["user"]["username"] ?></a>
                <span class="right color--secondary__text"><?= $data["wasEdited"] ? $timestamp." [edited]" : $timestamp ?></span>
                <span class="card-title"><?= $data["title"] ?></span>
                <?php
                if ($data["image"] != ""):
                ?>
                <a href="../post/?id=<?= $data["postId"] ?>">
                    <div class="card-image">
                        <img src="../assets/posts/<?= $data["image"] ?>" alt="<?= $data["title"] ?>">
                    </div>
                </a>
                <?php
                elseif ($data["description"] != ""):
                ?>
                <p class="truncate color--secondary__text"><?= $data["description"] ?></p>
                <?php
                else:
                ?>
                <p>[Unknown or deleted content]</p>
                <?php
                endif;
                ?>
            </div>
            <div class="card-action color--secondary">
                <a href="../posts/upvote.php?id=<?= $data["postId"] ?>&loc=home"><i class="material-icons <?= $data["hasUpvoted"] ? "aurora-text-green" : "color--primary__text" ?>">arrow_upward</i><span class="<?= $data["hasUpvoted"] ? "aurora-text-green" : "color--primary__text" ?>"><?= $data["upvotes"] ?></span></a>
                <a href="../posts/downvote.php?id=<?= $data["postId"] ?>&loc=home"><i class="material-icons <?= $data["hasDownvoted"] ? "aurora-text-red" : "color--primary__text" ?>">arrow_downward</i><span class="<?= $data["hasDownvoted"] ? "aurora-text-red" : "color--primary__text" ?>"><?= $data["downvotes"] ?></span></a>
                <!-- Uncomment when done coding
                <a href="../post/<?= $data["postId"] ?>/#comments" class="right"><i class="material-icons frost-text-cyan">chat</i>&nbsp;<span class="frost-text-cyan"><?= $data["comments"] ?></span></a>
                -->
                <a href="../post/?id=<?= $data["postId"] ?>" class="right"><i class="material-icons color--primary__text">info</i></a>
            </div>
        </div>
    </a>
</div>
