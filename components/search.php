<?php
if (!isset($placeholder)){
  header("Location: ../home");
  exit();
}
?>
<script src="../scripts/search.js" type="text/javascript"></script>
<div class="input-field col m7 s12">
    <i class="material-icons prefix color--primary__text">search</i>
    <input placeholder="<?= $placeholder ?>" id="searchTerm" type="text" class="color--primary__text" onkeyup="search()">
    <span class="helper-text color--secondary__text" data-error="Search term can only contain letters and spaces!"></span>
</div>