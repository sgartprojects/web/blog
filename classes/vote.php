<?php
class Vote {
    private Database $database;
    private int $postId;
    private int $userId;

    public function __construct(int $postId,int $userId = null,  Database $database = null)
    {
        $this->postId = $postId;

        if ($userId)
            $this->userId = $userId;
        else
            $this->userId = 0;

        if (!$database){
            include_once("../classes/database.php");
            $this->database = new Database();
        }
        else
            $this->database = $database;
    }

    public function hasUpvoted():bool
    {
        if ($this->userId > 0)
            if ($this->getUpvotes() > 0)
                return true;
        return false;
    }

    public function hasDownvoted():bool
    {
        if ($this->userId > 0)
            if ($this->getDownvotes() > 0)
                return true;
        return false;
    }

    public function getUpvotes():int
    {
        if ($this->userId > 0)
            return $this->database->read("SELECT Count(vote_id) as amount FROM vote WHERE type = 'up' AND post_id = ? AND user_id = ?", [$this->postId, $this->userId])[0]["amount"];
        else
            return $this->database->read("SELECT Count(vote_id) as amount FROM vote WHERE type = 'up' AND post_id = ?", [$this->postId])[0]["amount"];
    }

    public function getDownvotes():int
    {
        if ($this->userId > 0)
            return $this->database->read("SELECT Count(vote_id) as amount FROM vote WHERE type = 'down' AND post_id = ? AND user_id = ?", [$this->postId, $this->userId])[0]["amount"];
        else
            return $this->database->read("SELECT Count(vote_id) as amount FROM vote WHERE type = 'down' AND post_id = ?", [$this->postId])[0]["amount"];
    }

    public function upvote(){
        $upvotes = $this->getUpvotes();
        $downvotes = $this->getDownvotes();

        if ($upvotes == 0 && $downvotes == 0) {
            $this->insertUpvote();
        }

        if ($upvotes == 1){
            $this->removeUpvote();
        }

        if ($downvotes == 1){
            $this->removeDownvote();
            $this->insertUpvote();
        }
    }

    public function downvote()
    {
        $upvotes = $this->getUpvotes();
        $downvotes = $this->getDownvotes();

        if ($upvotes == 0 && $downvotes == 0) {
            $this->insertDownvote();
        }

        if ($downvotes == 1){
            $this->removeDownvote();
        }

        if ($upvotes == 1){
            $this->removeUpvote();
            $this->insertDownvote();
        }
    }

    private function insertUpvote(){
        $this->database->write("INSERT INTO vote (user_id, post_id, type) VALUES (?, ?, ?)", [$this->userId, $this->postId, 'up']);
    }

    private function insertDownvote(){
        $this->database->write("INSERT INTO vote (user_id, post_id, type) VALUES (?, ?, ?)", [$this->userId, $this->postId, 'down']);
    }

    private function removeUpvote(){
        $this->database->write("DELETE FROM vote WHERE post_id = ? AND user_id = ? AND type = ?", [$this->postId, $this->userId, 'up']);
    }

    private function removeDownvote(){
        $this->database->write("DELETE FROM vote WHERE post_id = ? AND user_id = ? AND type = ?", [$this->postId, $this->userId, 'down']);

    }
}