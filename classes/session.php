<?php
class Session {
    public bool $isValid;

    public function __construct(){
        $this->isValid = true;
    }

    public function createSession(array $sessionArgs = null){
        $this->openSession();
        $_SESSION["ipAddress"] = $_SERVER["REMOTE_ADDR"];
        $_SESSION["userAgent"] = $_SERVER["HTTP_USER_AGENT"];
        $_SESSION["lastAccess"] = time();
        for ($i = 0; $i < sizeof($sessionArgs); $i++)
            $_SESSION[$sessionArgs[$i]["key"]] = $sessionArgs[$i]["value"];
    }

    public function openSession(){
        session_start();

        // Validate session
        $this->validateSession();

        // Destroy session if invalid
        if (!$this->isValid) {
            session_destroy();
            session_start();
        }
    }

    public function validateSession(){
        // Check for same IP
        if (!isset($_SESSION["ipAddress"]) || $_SESSION["ipAddress"] != $_SERVER["REMOTE_ADDR"])
            $this->isValid = false;

        // Check for same User Agent
        if (!isset($_SESSION["userAgent"]) || $_SESSION["userAgent"] != $_SERVER["HTTP_USER_AGENT"])
            $this->isValid = false;

        // Check for session age
        if (!isset($_SESSION["lastAccess"]) || ($_SESSION["lastAccess"] + 84600) < time())
            $this->isValid = false;
        else
            $_SESSION["lastAccess"] = time();
    }
}