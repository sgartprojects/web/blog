<?php
class Post {
    private Database $database;
    private string $postId;
    private string $userId;
    private DateTime $createdAt;
    private DateTime $lastChangedAt;
    private bool $isArchived;
    private string $imagePath;
    private string $title;
    private string $description;
    private Vote $votes;
    private Vote $userVotes;

    public function __construct(string $postId, string $userId, DateTime $createdAt, DateTime $lastChangedAt,
                                bool $isArchived, string $imagePath, string $title, string $description,
                                Database $database = null)
    {
        $this->postId = $postId;
        $this->userId = $userId;
        $this->createdAt = $createdAt;
        $this->lastChangedAt = $lastChangedAt;
        $this->isArchived = $isArchived;
        $this->imagePath = $imagePath;
        $this->title = $title;
        $this->description = $description;
        if ($database)
            $this->database = $database;
        else
            include_once("../classes/database.php");
            $this->database = new Database();

        include_once("../classes/vote.php");
        if (isset($_SESSION["userId"]))
            $this->userVotes = new Vote($this->postId, $_SESSION["userId"], $this->database);
        else
            $this->userVotes = new Vote($this->postId, null, $this->database);
        $this->votes = new Vote($this->postId, null, $this->database);
    }

    public function getFeedData():array
    {
        return array(
            "postId" => $this->postId,
            "user" => $this->getUser(),
            "createdAt" => $this->createdAt,
            "wasEdited" => $this->wasEdited(),
            "isArchived" => $this->isArchived,
            "image" => $this->imagePath,
            "title" => $this->title,
            "description" => $this->description,
            "upvotes" => $this->votes->getUpvotes(),
            "downvotes" => $this->votes->getDownvotes(),
            "comments" => $this->getCommentsAmount(),
            "hasUpvoted" => $this->userVotes->hasUpvoted(),
            "hasDownvoted" => $this->userVotes->hasDownvoted()
        );
    }

    public function wasEdited():bool
    {
        return !($this->createdAt == $this->lastChangedAt);
    }

    public function getUser():array
    {
        include_once("../classes/user.php");
        $user = new User();
        return $user->getUserById($this->userId);
    }

    public function update(){
        // Update post in DB
    }

    public function getCommentsAmount():int
    {
        return $this->database->read("SELECT Count(comment_id) as amount FROM comment WHERE post_id = ?", [$this->postId])[0]["amount"];
    }
}