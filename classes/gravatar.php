<?php
class Gravatar{
    public string $hash;

    public function __construct(string $email = null)
    {
        if (!isset($email)){
            $email = "";
        }
        $this->hash = md5(strtolower(trim($email)));
    }

    public function getGravatar(): string
    {
        return "https://www.gravatar.com/avatar/$this->hash?d=identicon";
    }
}