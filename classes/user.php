<?php
class User {
    private Database $database;

    public function __construct(Database $database = null)
    {
        if (!$database){
            include_once("../classes/database.php");
            $this->database = new database();
        }
        else
            $this->database = $database;
    }

    public function isAdmin($username) {
        $data = $this->database->read("SELECT admin FROM user WHERE username = '$username'");
        if (sizeof($data) > 1) {
            die("Multiple users found! <br><a href='../home'>Back to homepage</a>");
        }
        foreach ($data as $user) {
            return $user["admin"];
        }
    }

    public function getUserByName($username): ?array
    {
        $rows = $this->database->read("SELECT user_id FROM user WHERE username = '$username'");
        foreach ($rows as $row) {
            $id = $row["user_id"];
        }
        if (isset($id)) {
            return $this->getUserById($id);
        }
        return null;
    }

    public function getUserById($userId): array
    {
        foreach ($this->database->read("SELECT * FROM user WHERE user_id = $userId") as $row) {
            $userId = $row["user_id"];
            $username = $row["username"];
            $firstname = $row["firstname"];
            $lastName = $row["last_name"];
            $email = $row["email"];
            $admin = $row["admin"];
            $active = $row["active"];
            $createDate = $row["created_at"];
            $hash = $row["user_hash"];
            $displayName = $row["display_name"];
        }
        if (isset($userId, $username, $firstname, $firstname, $lastName, $email, $admin, $active, $createDate, $hash, $displayName)) {
            return array(
                "userId" => $userId,
                "username" => $username,
                "firstname" => $firstname,
                "lastname" => $lastName,
                "email" => $email,
                "isAdmin" => $admin,
                "isActive" => $active,
                "createDate" => $createDate,
                "hash" => $hash,
                "displayName" => $displayName
            );
        }
        return array(
                "userId" => 0,
                "username" => '[deleted]',
                "firstname" => 'NONE',
                "lastname" => 'NONE',
                "email" => 'NONE',
                "isAdmin" => false,
                "isActive" => false,
                "createDate" => date_create("1970.01.01 00:00:00"),
                "hash" => 'NONE',
                "displayName" => 'username'
            );
    }

    function updateDisplayName($user){
        $_SESSION["displayName"] = "";
        if (isset($user["displayName"])){
            if ($user["displayName"] == "realName"){
                if (isset($user["firstname"]) && isset($user["lastname"])){
                    $_SESSION["displayName"] = $user["firstname"]." ".$user["lastname"];
                }
            }
            else if ($user["displayName"] == "username"){
                $_SESSION["displayName"] = $user["username"];
            }
            else{
                $_SESSION["displayName"] = $user["displayName"];
            }
        }
    }

    public function checkLogin(string $username, string $password):array
    {
        $user = $this->getUserByName($username);
        if (!$user)
            return array(
                "success" => false,
                "message" => "Invalid credentials!"
            );
        if (!$user["isActive"]){
            return array(
                "success" => false,
                "message" => "Your account is suspended!"
            );
        }
        $hash = hash('sha256', $username.'-'.$password);
        if ($user["hash"] == $hash){
            $this->updateDisplayName($user);
            return array(
                "success" => true,
                "message" => "Login successful!",
                "user" => $user
            );
        }
        else if ($user["hash"] != $hash)
            return array(
                "success" => false,
                "message" => "Invalid credentials!"
            );
        else
            return array(
                "success" => false,
                "message" => "Something went wrong!"
            );
    }
}
