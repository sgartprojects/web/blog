<?php
require_once('../classes/session.php');
$sessionHandler = new Session();
$sessionHandler->openSession();
if (isset($_GET["search"]) && $_GET["search"] != ""){
    $term = $_GET["search"];
}
else if (isset($_POST["term"]) && $_POST["term"] != ""){
    $term = $_POST["term"];
}
else if (isset($_GET["item"])){
    $item = $_GET["item"];
}

?>
<html lang="en">
<head>
    <!-- Title -->
    <title>Faq - Blog</title>

    <!-- Icon -->
    <link rel="icon" type="img/png" href="../assets/logo.png"

    <!-- Meta -->
    <meta name="author" content="sgart">
    <meta name="description" content="Blog">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="../css/index.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- JS for theming -->
    <script src="../scripts/utils.js" type="text/javascript"></script>

    <!-- JS for sidenav -->
    <script src="../scripts/materialize.min.js" type="text/javascript"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var themeSelector = document.querySelectorAll('select');
            M.FormSelect.init(themeSelector);
            const collapsible = document.querySelectorAll('.collapsible');
            M.Collapsible.init(collapsible);
            <?php
            if (isset($item)):
            ?>
            let instance = M.Collapsible.getInstance(document.getElementById('searchList'));
            instance
                .open(<?= $item ?>);
            <?php
            endif;
            ?>
        });
    </script>
</head>
<body class="color--primary">
    <?php
    include("../components/navigation.php");
    ?>
    <main>
        <div class="row hide-on-med-and-down"></div>
        <div class="row">
            <div class="col s0 m2 hide-on-med-and-down"></div>
            <?php
            $placeholder = "Search for topic";
            include("../components/search.php");
            ?>
        </div>
        <div class="row">
            <div class="col s0 m2 hide-on-med-and-down"></div>
            <div class="col s12 m7">
                <ul class="collapsible popout" id="search-list">
                    <?php
                    include("../classes/faq.php");
                    if (!isset($db))
                        $faq = new FaQ();
                    else
                        $faq = new FaQ($db);
                    foreach($faq->getAllItems() as $item){
                        include("../components/faqitem.php");
                    }
                    ?>
                </ul>
            </div>
        </div>
    </main>
</body>
</html>
